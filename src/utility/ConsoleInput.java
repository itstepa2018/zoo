package utility;

import animals.Animal;
import interfaces.Input;

import java.math.BigDecimal;
import java.util.InputMismatchException;
import java.util.Scanner;

public class ConsoleInput implements Input {
    private Scanner scanner = new Scanner(System.in);

    @Override
    public Integer getInt(String message) {
        System.out.println(message);
        try {
            return Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException exc) {
            throw new InputMismatchException();
        }
    }

    @Override
    public String getString(String message) {
        System.out.println(message);
        return scanner.nextLine();
    }

    @Override
    public Boolean getBoolean(String message) {
        System.out.println(message);
        String input = scanner.nextLine();
        return input.equalsIgnoreCase("yes") || input.equalsIgnoreCase("y")
                || input.equalsIgnoreCase("true") || input.equals("1");
    }

    @Override
    public Double getDouble(String message) {
        System.out.println(message);
        String string = scanner.nextLine();
        try {
            BigDecimal bigDecimal = new BigDecimal(string.replaceAll(" ", ".")).setScale(Animal.SCALE, BigDecimal.ROUND_UNNECESSARY);
            return bigDecimal.doubleValue();
        } catch (NullPointerException | NumberFormatException exc) {
            throw new InputMismatchException();
        }
    }
}
