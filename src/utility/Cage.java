package utility;

import animals.Animal;
import animals.Herbivore;
import animals.Predator;

import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Cage<T extends Animal> {
    private List<T> list = new LinkedList<>();

    public void addAnimal(T animal) {
        if (animal != null) {
            if (animal instanceof Herbivore) {
                List<Predator> listPredator = getPredatorList();
                if (listPredator.size()> 0  ){
                 listPredator.get(0).consume(animal);
                }
            }
            list.add(animal);
        }
    }

    public List<Predator> getPredatorList(){
        List<Predator> listPredator = new LinkedList<>();
                for (Animal animal: list) {
                    if (animal instanceof Predator) {
                        listPredator.add((Predator) animal);
                    }
                }
                listPredator.sort(new Comparator<Predator>() {
                    @Override
                    public int compare(Predator o1, Predator o2) {
                        if(){

                        }
                        return 0;
                    }
                });
        return listPredator;
    }

    public T getAnimal(int index) {
        return list.get(index);
    }

    public ChallengeResult jumpChallenge() {
        double maxDistance = 0;
        Animal animal = null;
        for (T item : list) {
            double distance = item.jump();
            if (maxDistance < distance) {
                animal = item;
                maxDistance = distance;
            }
        }
        return new ChallengeResult(animal, maxDistance);
    }

    @Override
    public String toString() {
        if (list.size() == 0) {
            return "Cage is empty";
        }
        StringBuilder stringBuilder = new StringBuilder("Info: ");
        for (T item : list) {
            stringBuilder.append("\n");
            stringBuilder.append(item.toString());
        }
        return stringBuilder.toString();
    }

    public static class ChallengeResult {
        private Animal animal;
        private double jump;

        public ChallengeResult(Animal animal, double jump) {
            this.animal = animal;
            this.jump = jump;
        }

        public Animal getAnimal() {
            return animal;
        }

        public double getJump() {
            return jump;
        }

        @Override
        public String toString() {
            return "ChallengeResult{" +
                    "animal=" + animal +
                    ", jump=" + jump +
                    '}';
        }
    }

    private static class AnimalComparator<T extends Animal> implements Comparator<T> {
        @Override
        public int compare(T o1, T o2) {
            if (o1.getSize() == o2.getSize()) {
                return 0;
            } else if (o1.getSize() < o2.getSize()) {
                return -1;
            } else return 1;
        }
    }


}

