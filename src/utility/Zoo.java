package utility;

import animals.*;
import exception.AnimalCreationException;
import interfaces.Input;


public class Zoo {
    private Input inputSource = new ConsoleInput();
    private Cage<Animal> animalCage = new Cage<>();
    private AbstractAnimalFactory factory = new ConsoleAnimalFactory();
    StringBuilder sB = new StringBuilder();

    private void start() {
        boolean isContinue = true;
        StringBuilder sb = new StringBuilder();
        while (isContinue) {
            sb.setLength(0);
            sb.append("What do you want to do?(͡°͜ʖ͡°)\n");
            sb.append("1 - Add Animal\n");
            sb.append("2 - Delete Animal\n");
            sb.append("3 - Feed Animal\n");
            sb.append("4 - Show Info\n");
            sb.append("5 - Challenge\n");
            sb.append("0 - Exit");
            final String input = inputSource.getString(sb.toString());
            switch (input) {
                case "1":
                    animalCage.addAnimal(createAnimal());
                    break;
                case "2":
                    System.out.println("Under construction");
                    break;
                case "3":
                    System.out.println("Under construction");
                    break;
                case "4":
                    showInfo();
                    break;
                case "5":
                    jumpChallenge();
                    break;
                case "0":
                    isContinue = false;
                    break;
                default:
                    System.out.println("Try again");
            }
        }
    }

    private void jumpChallenge() {
        Cage.ChallengeResult challengeResult =
                animalCage.jumpChallenge();
        System.out.println(challengeResult);
    }

    private void showInfo() {
        System.out.println(animalCage);
    }

    private Animal createAnimal() {
        String s = "1 - Parrot\n"
                + "2 - Cat\n"
                + "3 - Wolf\n"
                + "4 - Rabbit\n"
                + "Any other key - Back";
        String input = inputSource.getString(s);
        try {
            switch (input) {
                case "1":
                    return factory.createAnimal(Parrot.class.getSimpleName());
                case "2":
                    return factory.createAnimal(Cat.class.getSimpleName());
                case "3":
                    return factory.createAnimal(Wolf.class.getSimpleName());
                case "4":
                    return factory.createAnimal(Rabbit.class.getSimpleName());
                default:
                    return null;
            }
        } catch (AnimalCreationException exception) {
            System.out.println(exception.getMessage());
            return null;
        }
    }

    public static void main(String[] args) {
        final Zoo zoo = new Zoo();
        zoo.start();
        System.out.println();
        System.out.println("Bye");
    }
}

