package interfaces;

public interface Input {
    Integer getInt(String message);

    String getString(String message);

    Boolean getBoolean(String message);

    Double getDouble(String message);
}
