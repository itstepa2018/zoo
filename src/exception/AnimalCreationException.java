package exception;

public class AnimalCreationException extends RuntimeException {
    public AnimalCreationException(String message) {
        super(message);
    }
}
