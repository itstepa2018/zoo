package animals;

public abstract class Predator extends Mammal {
    public boolean isScavenger;

    public Predator(String nickName, double size) {
        super(nickName, size);
    }

    public void consume (Animal animal){
        setSatiety(getSatiety() + animal.getSize()/2);
    }



    @Override
    public String toString() {
        return super.toString()+", isScavenger: "+isScavenger;
    }
}
