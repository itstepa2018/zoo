package animals;

public abstract class Feline extends Predator {
    private int breed;

    public Feline(String nickName, double size) {
        super(nickName, size);
        bloodTemperature = 41;
    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }
}
