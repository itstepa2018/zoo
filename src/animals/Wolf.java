package animals;

public final class Wolf extends Predator {
    private int quantityRedCapsEaten;

    public Wolf(String nickName, double size) {
        super(nickName, size);
    }

    @Override
    public void sound() {
        System.out.println("woof woof");
    }

    public void setQuantityRedCapsEaten(int quantityRedCapsEaten) {
        this.quantityRedCapsEaten = quantityRedCapsEaten;
    }

    public static class Builder {
        private Wolf wolf;

        public Builder() {
            wolf = new Wolf("Vasya", 2.0);
        }

        public Wolf.Builder nickName(String nickName) {
            wolf.setNickName(nickName);
            return this;
        }

        public Wolf.Builder size(double size) {
            wolf.setSize(size);
            return this;
        }

        public Wolf.Builder eatenRedCaps(int redCapsQuatity) {
            wolf.setQuantityRedCapsEaten(redCapsQuatity);
            return this;
        }

        public Wolf build(){
            return wolf;
        }
    }
}
