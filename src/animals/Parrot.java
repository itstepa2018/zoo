package animals;

public class Parrot extends Bird {
    public static final String KESHA = "Kesha";
    private boolean canSpeak;
    private boolean canFly;

    private Parrot(String nickName, double size, boolean canSpeak, boolean canFly) {
        super(nickName, size);
        this.canSpeak = canSpeak;
        this.canFly = canFly;
    }

    public boolean isCanSpeak() {
        return canSpeak;
    }

    public void setCanSpeak(boolean canSpeak) {
        this.canSpeak = canSpeak;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public void setCanFly(boolean canFly) {
        this.canFly = canFly;
    }

    @Override
    public String toString() {
        return super.toString() +
                " canSpeak=" + canSpeak +
                ", canFly=" + canFly;
    }

    public static final class Builder {
        private Parrot parrot;

        Builder() {
            parrot = new Parrot(KESHA + " #" + Animal.getIdCounter(), 3, false, true);
        }

        public Parrot.Builder nickName(String s) {
            parrot.setNickName(s);
            return this;
        }

        public Parrot.Builder size(double s) {
            parrot.setSize(s);
            return this;
        }

        public Parrot.Builder canFly(boolean cf) {
            parrot.setCanFly(cf);
            return this;
        }

        public Parrot.Builder canSpeak(boolean cs) {
            parrot.setCanSpeak(cs);
            return this;
        }

        public Parrot build() {
            return parrot;
        }
    }
}
