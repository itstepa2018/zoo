package animals;


public class Cat extends Feline {
    public static final int BREED_UNKNOWN = 0;
    public static final int BREED_ANGORA = 1;
    public static final int BREED_MANUL = 2;
    private int breed;
    private final String SOUND = "meow";

    Cat(String nickName, double size) {
        super(nickName, size);
        bloodTemperature = 41;
    }

    @Override
    public void sound() {
        System.out.println(SOUND);
    }

    @Override
    public double jump() {
        return 13 / getSize();
    }

    public static class Builder {
        private Cat cat;

        public Builder() {
            cat = new Cat("Barsik", .8);
        }

        public Cat.Builder nickName(String nickName) {
            cat.setNickName(nickName);
            return this;
        }

        public Cat.Builder size(double size) {
            cat.setSize(size);
            return this;
        }

        public Cat build() {
            return cat;
        }


    }

    public int getBreed() {
        return breed;
    }

    public void setBreed(int breed) {
        this.breed = breed;
    }
}

