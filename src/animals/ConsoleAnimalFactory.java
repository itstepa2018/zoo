package animals;

import exception.AnimalCreationException;
import utility.ConsoleInput;

import java.util.InputMismatchException;

public class ConsoleAnimalFactory extends AbstractAnimalFactory {

    public ConsoleAnimalFactory() {
        this.input = new ConsoleInput();
    }

    @Override
    public Animal createAnimal(String animalType) {
        try {
            if (animalType.equalsIgnoreCase(Wolf.class.getSimpleName())) {
                String nickName = input.getString("Input nickname");
                double size = input.getDouble("Input size");
                return new Wolf.Builder().nickName(nickName).size(size).build();
            } else if (animalType.equalsIgnoreCase(Rabbit.class.getSimpleName())) {
                String nickName = input.getString("Input nickname");
                double size = input.getDouble("Input size");
                return new Rabbit.Builder().nickName(nickName).size(size).build();
            } else if (animalType.equalsIgnoreCase(Parrot.class.getSimpleName())) {
                String nickName = input.getString("Input nickname");
                double size = input.getDouble("Input size");
                boolean isSpeak = input.getBoolean("Can your parrot speak?");
                return new Parrot.Builder().nickName(nickName).size(size).canSpeak(isSpeak).build();
            } else if (animalType.equalsIgnoreCase(Cat.class.getSimpleName())) {
                String nickName = input.getString("Input nickname");
                double size = input.getDouble("Input size");
                Cat c = new Cat(nickName, size);
                c.setBreed(Cat.BREED_MANUL);
                return c;
            } else {
                throw new AnimalCreationException("Animal can not be created: " +
                        animalType);
            }
        } catch (InputMismatchException exc) {
            throw new AnimalCreationException("Input error - Animal can not be " +
                    "created");
        }
    }
}
