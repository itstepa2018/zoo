package animals;

public abstract class Bird extends Animal {
    private double wingSpan;
    private String color = "gray";

    public Bird(String nickName, double size) {
        super(nickName, size);
        wingSpan = size * .2;
    }

    @Override
    public double jump() {
        return 1 / getSize();
    }

    public final void sound() {
        System.out.println("Piy Piy");
    }

    public void fly() {
        fly(0);
    }

    public void fly(double windForce) {
        double heightFly = (this.wingSpan / this.getSize()) * 100 - windForce;
        System.out.println("Height fly: " + heightFly);
    }

    public double getWingSpan() {
        return wingSpan;
    }

}
