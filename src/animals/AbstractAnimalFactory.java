package animals;

import interfaces.Input;

public abstract class AbstractAnimalFactory {
    Input input;

    public abstract Animal createAnimal(String animalType);

}
