package animals;

public abstract class Mammal extends Animal {
   protected double bloodTemperature;

    public Mammal(String nickName, double size) {
        super(nickName, size);
        bloodTemperature = 38;
    }

    public double getBloodTemperature() {
        return bloodTemperature;
    }

}
