package animals;

import interfaces.Jumpable;
import interfaces.Soundable;

import java.math.BigDecimal;
import java.util.Objects;

public abstract class Animal implements Soundable, Jumpable {
    public static final int SCALE = 5;
    private int id;
    private static int idCounter;
    public static String type = "Animal";
    private String nickName;
    private BigDecimal size;
    public String color = "transparent";
    private double satiety;

    public Animal(String nickName, double size) {
        this.size = new BigDecimal(size).setScale(SCALE, BigDecimal.ROUND_UNNECESSARY);
        id = idCounter++;
        this.nickName = nickName;
        id = idCounter++;
        this.satiety = 80;
    }

    public void setSatiety(double satiety) {
        this.satiety = satiety;
    }

    public double getSatiety() {
        return satiety;
    }

    @Override
    public void sound() {

    }

    @Override
    public double jump() {
        return 0;
    }

    public void setSize(double size) {
        this.size = new BigDecimal(size).setScale(SCALE, BigDecimal.ROUND_UNNECESSARY);
    }

    public static Animal createAnimal() {
        return null;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public String getNickName() {
        return nickName;
    }

    public double getSize() {
        return size.doubleValue();
    }

    public final static String getType() {
        return type;
    }

    public static int getIdCounter() {
        return idCounter;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Animal)) return false;
        Animal animal = (Animal) o;
        return id == animal.id;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return (this.getClass().getSimpleName() + " " + this.nickName
                + ", size: " + String.format("%.2f", size.doubleValue()) + id);
    }
}
