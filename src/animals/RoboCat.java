package animals;

public class  RoboCat extends Feline {
    private static final double DEFAULT_SIZE = 4.1;
    private int objectCounter = 0;
    private VoiceModule voiceModule;

    public RoboCat(String sound) {
        super("RoboCat #", DEFAULT_SIZE);
        this.voiceModule = new VoiceModule(sound);
        objectCounter++;
    }

    @Override
    public void sound() {
        System.out.println(voiceModule.getSound());
    }

    public static class VoiceModule {
        private String sound;

        public VoiceModule(String sound) {
            setSound(sound);
        }

        public String getSound() {
            return this.sound;
        }

        public void setSound(String sound) {
            this.sound = sound;
        }
    }
}
