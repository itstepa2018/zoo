package animals;


public class Rabbit extends Herbivore {
    public Rabbit(String nickName, double size) {
        super(nickName, size);
    }

    @Override
    public double jump() {
        return 14 / getSize();
    }

    @Override
    public void sound() {
        System.out.println("Fr-fr-fr");
    }

    public static class Builder{
        private Rabbit rabbit;

        public Builder(){
            rabbit = new Rabbit("Rodger",.3);
        }

        public Rabbit.Builder nickName(String nickName){
            rabbit.setNickName(nickName);
            return this;
        }
        public Rabbit.Builder size(double size){
            rabbit.setSize(size);
            return this;
        }
        public Rabbit build(){
            return rabbit;
        }
    }

}
